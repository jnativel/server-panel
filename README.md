Serveur dédié - Panel de gestion
==========================================

Ce script permet d'installer un panel de gestion pour notre serveur dédié.

### Pré-requis :

- ``server-autoinstall``

### Installation

```bash
apt-get update && apt-get dist-upgrade
apt-get install git -y
```

```bash
cd /tmp
git clone https://gitlab.com/jnativel/server-panel.git
cd server-panel
chmod +x install.sh
./install.sh
```

