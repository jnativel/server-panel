#!/bin/bash
# ---------------------------------------
# Codes couleurs
# ---------------------------------------
CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"
CYELLOW="${CSI}1;33m"
CPURPLE="${CSI}1;35m"
CCYAN="${CSI}1;36m"
CBROWN="${CSI}0;33m"
#----------------------------------------
# Functions
#----------------------------------------
function gen_user () {
    echo "$1" | sed -e 's/[^a-z]//g' | cut -c -8
}
function gen_pwd () {
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-12} | head -n 1
}
#----------------------------------------
# Test des variables
#----------------------------------------
DOMAIN=$1
USER=`gen_user $DOMAIN`
USER_HOME="/home/"$USER
USER_PWD=`gen_pwd`
if [ -z "$DOMAIN" ]; then
    
    exit 1
fi
#----------------------------------------
# Affichage
#----------------------------------------
echo -e "${CBROWN}----------------${CEND}"
echo -e "${CBROWN}DOMAIN:${CEND} ${CGREEN}${DOMAIN}${CEND}"
echo -e "${CBROWN}USER:${CEND}   ${CGREEN}${USER}${CEND}"
echo -e "${CBROWN}PWD:${CEND}    ${CGREEN}${USER_PWD}${CEND}"
echo -e "${CBROWN}----------------${CEND}"
#----------------------------------------
# Création
#----------------------------------------
echo -e "${CGREEN}-> Création de l'utilisateur${CEND}"
if [ -z "$(getent passwd $USER)" ];then
    useradd -r --shell /bin/false $USER
    usermod -aG www-data $USER
fi
echo -e "${CGREEN}-> Création des répertoires${CEND}"
if [ ! -d "$USER_HOME" ];then
    mkdir $USER_HOME && echo $USER_HOME;
    if [ ! -d "$USER_HOME/backup" ];then
            mkdir $USER_HOME/backup && echo "$USER_HOME/backup";
    fi
    if [ ! -d "$USER_HOME/etc" ];then
            mkdir $USER_HOME/etc && echo "$USER_HOME/etc";
    fi
    if [ ! -d "$USER_HOME/mail" ];then
            mkdir $USER_HOME/mail && echo "$USER_HOME/mail";
    fi
    if [ ! -d "$USER_HOME/tmp" ];then
            mkdir $USER_HOME/tmp && echo "$USER_HOME/tmp";
    fi
    if [ ! -d "$USER_HOME/www" ];then
            mkdir $USER_HOME/www && echo "$USER_HOME/www";
    fi
    if [ ! -d "$USER_HOME/www/httpdocs" ];then
            mkdir $USER_HOME/www/httpdocs && echo "$USER_HOME/www/httpdocs";
    fi
    if [ ! -d "$USER_HOME/www/logs" ];then
            mkdir $USER_HOME/www/logs && echo "$USER_HOME/www/logs";
    fi
    if [ ! -d "$USER_HOME/www/sd" ];then
            mkdir $USER_HOME/www/sd && echo "$USER_HOME/www/sd";
    fi
fi
echo -e "${CGREEN}-> Création des fichiers par défaut${CEND}"
echo "<?php phpinfo(); ?>" > /home/$USER/www/httpdocs/info.php
echo "${DOMAIN}" > /home/$USER/www/httpdocs/index.php
echo -e "${CGREEN}-> Modification des permissions${CEND}"
chown -R www-data:www-data $USER_HOME
find $USER_HOME -type d -exec chmod 0755 {} +
find $USER_HOME -type f -exec chmod 0664 {} +
#----------------------------------------
# nginx
#----------------------------------------
echo -e "${CGREEN}-> Création du vhost: /etc/nginx/conf.d/${DOMAIN}.conf ${CEND}"
cp conf/vhost.conf /etc/nginx/conf.d/$DOMAIN.conf
sed -i -e "s|%DOMAIN%|$DOMAIN|g"   \
       -e "s|%USER%|$USER|g" /etc/nginx/conf.d/$DOMAIN.conf
#----------------------------------------
# Redemarrage
#----------------------------------------
service nginx restart
