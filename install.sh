
#!/bin/bash
# Auteur Jimmy NATIVEL <jimmy@nativel.fr>
# ---------------------------------------
# Affectations des variables
# ---------------------------------------
OS_VERSION=`cat /etc/debian_version | cut -c 1`
CODENAME=`cat /etc/*-release | grep "VERSION="`
CODENAME=${CODENAME##*\(}
CODENAME=${CODENAME%%\)*}
echo -n "PANEL port: "
read PANEL_PORT
if [ ! "$(echo $PANEL_PORT | grep "^[ [:digit:] ]*$")" ]; then
	echo "PANEL port incorrect"
	exit 1
fi
echo -n  "PANEL utilisateur: "
read PANEL_USER
if [ -z "$PANEL_USER" ]; then
        echo "Utilisateur incorrect"
        exit 1
fi
echo -n "PANEL mot de passe: "
read -s PANEL_PWD && echo
if [ -z "$PANEL_PWD" ]; then
        echo "Mot de passe incorrect"
        exit 1
fi
echo -n "(Vérification) "
read -s PANEL_PWD2 && echo
if [ ! "$PANEL_PWD" == "$PANEL_PWD2" ]; then
        echo "Les mots de passes ne sont pas identiques"
        exit 1
fi
# ---------------------------------------
# Test des variables
# ---------------------------------------
if [ "$(id -u)" != "0" ]; then
    echo "ERREUR: Ce script doit être exécuter par l'utilisateur root" 1>&2
    exit 1
fi
# ---------------------------------------
# Test des variables
# ---------------------------------------
if [ ! -n "$OS_VERSION" ]; then
    echo "ERREUR: Installation possible que sur les OS Debian" 1>&2
    exit 1
fi
# ---------------------------------------
# Installation
# ---------------------------------------
# Création du virtualhost pour Nginx
cp nginx/panel.conf /etc/nginx/conf.d/panel.conf
sed -i -e "s/%PANEL_PORT%/$PANEL_PORT/" /etc/nginx/conf.d/panel.conf
# Création des différents répertoires
mkdir /usr/share/nginx/panel && mkdir /usr/share/nginx/panel/www && mkdir /usr/share/nginx/panel/www/httpdocs && mkdir /usr/share/nginx/panel/www/logs
# Création des fichiers
echo "Panel" > /usr/share/nginx/panel/www/httpdocs/index.html
echo "<?php echo phpinfo(); ?>" > /usr/share/nginx/panel/www/httpdocs/phpinfo.php
cp /usr/share/doc/php-apc/apc.php /usr/share/nginx/panel/www/httpdocs
# Sécuriser l'accès au Panel
# Ouverture du port pour Fail2ban
cat firewall/panel.rules >> /root/firewall/default.rules
sed -i -e "s/%PANEL_PORT%/$PANEL_PORT/" /root/firewall/default.rules
# ---------------------------------------
# Rédémarrage des service
# ---------------------------------------
/etc/init.d/nginx restart
/etc/init.d/firewall.sh stop
/etc/init.d/firewall.sh start
